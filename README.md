# CloudBurst
[![godoc](https://godoc.org/gitlab.com/silkeh/cloudburst?status.svg)](https://godoc.org/gitlab.com/silkeh/cloudburst)
[![pipeline status](https://gitlab.com/silkeh/cloudburst/badges/master/pipeline.svg)](https://gitlab.com/silkeh/cloudburst/commits/master)
[![coverage report](https://gitlab.com/silkeh/cloudburst/badges/master/coverage.svg)](https://gitlab.com/silkeh/cloudburst/commits/master)
[![goreportcard](https://goreportcard.com/badge/gitlab.com/silkeh/cloudburst)](https://goreportcard.com/report/gitlab.com/silkeh/cloudburst)

CloudBurst is an HTTP/CoAP frontend for storing/retrieving [SenML][] in/from InfluxDB using a simple API.

The API (documented below) works over HTTP(s) and CoAP(s) with CBOR (`application/senml+cbor`),
JSON (`application/senml+json`), and XML (`application/senml+xml`).
[SenSML][] or [SenML FETCH/PATCH][] are currently not supported.

## Installation

Download and build the application using:

```sh
go get "gitlab.com/silkeh/cloudburst/cmd/cloudburst"
```

The server can be started using `cloudburst -c <config file>`.
Example configuration can be found in `config.dist.yaml`.

## API

The API exposes endpoints based on [InfluxDB measurements][].
This allows one to store data from different sources separately.
See the [OpenAPI specification](openapi.yaml) for details.

[SenML]: https://www.rfc-editor.org/rfc/rfc8428
[SensML]: https://www.rfc-editor.org/rfc/rfc8428#section-4.8
[SenML FETCH/PATCH]: https://www.rfc-editor.org/rfc/rfc8790
[InfluxDB measurements]: https://docs.influxdata.com/influxdb/v2.0/reference/key-concepts/data-elements/#measurement

## Docker

A Docker image is provided to make it easy to run cloudburst in Docker.
The [`docker`](docker) directory contains an example configuration for
running Cloudburst with HTTP and CoAP (udp and tcp) listeners against an InfluxDB2 backend.
Simply run `docker-compose up` from the directory to try it out.

Data can then be submitted over both HTTP and CoAP. For example:

```console
curl -D- \
     -X POST \
     -H 'Content-Type: application/json' \
     -d '[{"bn":"urn:dev:ow:10e2073a0108006:","bt":1.276020076001e+09,"bu":"A","bver":5,"n":"voltage","u":"V","v":120.1},{"n":"current","t":-5,"v":1.2},{"n":"current","t":-4,"v":1.3},{"n":"current","t":-3,"v":1.4},{"n":"current","t":-2,"v":1.5},{"n":"current","t":-1,"v":1.6},{"n":"current","v":1.7}]' \
     http://localhost:8080/api/v1
```

And to retrieve the data:

```console
curl -H "Accept: application/json" http://localhost:8080/api/v1
```
