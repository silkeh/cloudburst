package main

import (
	"bytes"
	"context"
	"flag"
	"io/ioutil"
	"log"
	"strings"
	"time"

	"github.com/plgd-dev/go-coap/v2/message"
	"github.com/plgd-dev/go-coap/v2/message/codes"
	"github.com/plgd-dev/go-coap/v2/udp"
	"github.com/silkeh/senml"
	"gitlab.com/silkeh/cloudburst/scrapers/buienradar"
)

func request(client *buienradar.Client, server, measurement string, stations ...string) {
	m, err := client.GetSenML(stations...)
	if err != nil {
		log.Printf("Error collecting Buienradar data: %s", err)
		return
	}

	b, err := senml.EncodeCBOR(m)
	if err != nil {
		log.Printf("Error serializing CBOR: %s", err)
		return
	}

	conn, err := udp.Dial(server, udp.WithErrors(func(err error) {})) // suppress error
	if err != nil {
		log.Printf("Error creating CoAP client: %s", err)
		return
	}
	defer conn.Close()

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	msg, err := conn.Post(ctx, "api/v1/"+measurement, message.AppCBOR, bytes.NewReader(b))
	if err != nil {
		log.Printf("Error sending CoAP: %s", err)
		return
	}

	if msg.Code() != codes.Created {
		body, _ := ioutil.ReadAll(msg.Body())
		log.Printf("Error response %v: %q", msg.Code(), body)
	}
}

func main() {
	var stations, server, measurement string
	var interval time.Duration

	flag.StringVar(&server, "server", "localhost:5683", "CoAP server")
	flag.StringVar(&measurement, "measurement", "buienradar", "Measurement name")
	flag.StringVar(&stations, "stations", "Twente", "Comma separated list of weather stations to collect")
	flag.DurationVar(&interval, "interval", 0, "Collection interval, 0 to scape only once.")
	flag.Parse()

	stationList := strings.Split(stations, ",")
	client := buienradar.NewClient(nil)
	request(client, server, measurement, stationList...)
	if interval == 0 {
		return
	}

	ticker := time.NewTicker(interval)
	for range ticker.C {
		request(client, server, measurement, stationList...)
	}
}
