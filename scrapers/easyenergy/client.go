package easyenergy

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"time"

	"github.com/silkeh/senml"
)

// Client is a HTTP client for the EasyEnergy API
type Client struct {
	*http.Client
}

// NewClient creates a new HTTP client for the EasyEnergy API
func NewClient(cli *http.Client) *Client {
	if cli == nil {
		cli = http.DefaultClient
	}
	return &Client{Client: cli}
}

// Electricity retrieves the latest electricity data from the EasyEnergy API
func (c *Client) Electricity(start, end time.Time) (data []EnergyTariff, err error) {
	return c.get(ElectricityURL, start, end)
}

// Gas retrieves the latest electricity data from the EasyEnergy API
func (c *Client) Gas(start, end time.Time) (data []EnergyTariff, err error) {
	return c.get(GasURL, start, end)
}

// get retrieves the latest data from the EasyEnergy API
func (c *Client) get(url string, start, end time.Time) (data []EnergyTariff, err error) {
	data = make([]EnergyTariff, 0)
	apiURL := fmt.Sprintf("%s?startTimestamp=%s&endTimestamp=%s",
		url, encodeTime(start), encodeTime(end))

	var resp *http.Response
	resp, err = c.Client.Get(apiURL)
	if err != nil {
		return
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 400 {
		var e Error
		err = json.NewDecoder(resp.Body).Decode(&e)
		if err != nil {
			return
		}
		return nil, e.Error()
	}

	err = json.NewDecoder(resp.Body).Decode(&data)
	return
}

// GetSenML collects measurements from EasyEnergy and converts them to SenML
func (c *Client) getSenML(prefix, url string, start, end time.Time) ([]senml.Measurement, error) {
	data, err := c.get(url, start, end)
	if err != nil {
		return nil, fmt.Errorf("error collecting EasyEnergy data: %w", err)
	}

	measurements := make([]senml.Measurement, 0, len(data)*2)
	for _, tariff := range data {
		p := fmt.Sprintf("%s:%v:", prefix, tariff.SupplierID)
		measurements = append(measurements,
			senml.NewValue(p+"tariff_usage", tariff.TariffUsage, senml.None, tariff.Time, 0),
			senml.NewValue(p+"tariff_return", tariff.TariffReturn, senml.None, tariff.Time, 0),
		)
	}

	return measurements, nil
}

// GetSenML collects measurements from EasyEnergy and converts them to SenML
func (c *Client) GetSenML(start, end time.Time) ([]senml.Measurement, error) {
	e, err := c.getSenML("electricity", ElectricityURL, start, end)
	if err != nil {
		return nil, fmt.Errorf("error collecting EasyEnergy electricity data: %w", err)
	}

	g, err := c.getSenML("gas", GasURL, start, end)
	if err != nil {
		return nil, fmt.Errorf("error collecting EasyEnergy gas data: %w", err)
	}

	return append(e, g...), nil
}

// encodeTime ElectricityURL-encodes a time for the EasyEnergy API.
func encodeTime(t time.Time) string {
	return url.QueryEscape(t.UTC().Format(time.RFC3339))
}
