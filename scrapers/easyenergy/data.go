package easyenergy

import (
	"time"
)

const (
	// ElectricityURL is the URL to the Easy Energy electricity JSON API
	ElectricityURL = "https://mijn.easyenergy.com/nl/api/tariff/getapxtariffs"

	// GasURL is the URL to the Easy Energy gas JSON API
	GasURL = "https://mijn.easyenergy.com/nl/api/tariff/getlebatariffs"
)

// EnergyTariff represents an hourly energy price from a supplier
type EnergyTariff struct {
	Time         time.Time `json:"Timestamp"`
	SupplierID   int       `json:"SupplierId"`
	TariffUsage  float64   `json:"TariffUsage"`
	TariffReturn float64   `json:"TariffReturn"`
}
