package server

import (
	"net"
	"net/http"
	"strings"

	"github.com/plgd-dev/go-coap/v2/message/codes"
)

// codeToHTTPStatus contains a mapping of CoAP error codes to HTTP status codes.
var codeToHTTPStatus = map[codes.Code]int{
	codes.Empty:                   http.StatusNoContent,
	codes.Created:                 http.StatusCreated,
	codes.Deleted:                 http.StatusOK,
	codes.Valid:                   http.StatusOK,
	codes.Changed:                 http.StatusOK,
	codes.Content:                 http.StatusOK,
	codes.Continue:                http.StatusContinue,
	codes.BadRequest:              http.StatusBadRequest,
	codes.Unauthorized:            http.StatusUnauthorized,
	codes.BadOption:               http.StatusBadRequest,
	codes.Forbidden:               http.StatusForbidden,
	codes.NotFound:                http.StatusNotFound,
	codes.MethodNotAllowed:        http.StatusMethodNotAllowed,
	codes.NotAcceptable:           http.StatusNotAcceptable,
	codes.RequestEntityIncomplete: http.StatusRequestedRangeNotSatisfiable,
	codes.PreconditionFailed:      http.StatusPreconditionFailed,
	codes.RequestEntityTooLarge:   http.StatusRequestEntityTooLarge,
	codes.UnsupportedMediaType:    http.StatusUnsupportedMediaType,
	codes.InternalServerError:     http.StatusInternalServerError,
	codes.NotImplemented:          http.StatusNotImplemented,
	codes.BadGateway:              http.StatusBadGateway,
	codes.ServiceUnavailable:      http.StatusServiceUnavailable,
	codes.GatewayTimeout:          http.StatusGatewayTimeout,
	codes.ProxyingNotSupported:    http.StatusHTTPVersionNotSupported,
}

// HTTPResponseWriter contains an HTTP based ResponseWriter.
type HTTPResponseWriter struct {
	http.ResponseWriter
}

// NewHTTPResponseWriter returns ResponseWriter based on a http.ResponseWriter.
func NewHTTPResponseWriter(w http.ResponseWriter) ResponseWriter {
	return &HTTPResponseWriter{w}
}

// SetResponse sets the response code, content type and data
func (r *HTTPResponseWriter) SetResponse(code codes.Code, contentType string, data []byte) error {
	r.WriteHeader(codeToHTTPStatus[code])
	r.Header().Set("Content-Type", contentType)
	_, err := r.Write(data)
	return err
}

// WriteErrors returns true if errors should be written to the client.
func (r *HTTPResponseWriter) WriteErrors() bool {
	return true
}

// NewHTTPRequest returns a Request based on an http.Request.
func NewHTTPRequest(r *http.Request) *Request {
	request := &Request{
		Method:      r.Method,
		Path:        strings.Split(strings.Trim(r.URL.Path, "/"), "/"),
		Query:       r.URL.Query(),
		ContentType: r.Header.Get("Content-Type"),
		Accept:      r.Header.Get("Accept"),
		Body:        r.Body,
		Confirmable: true,
	}

	request.RemoteAddr, _, _ = net.SplitHostPort(r.RemoteAddr)

	return request
}
