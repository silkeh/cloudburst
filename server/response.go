package server

import (
	"github.com/plgd-dev/go-coap/v2/message/codes"
)

// ResponseWriter is a generic response writer.
type ResponseWriter interface {
	// SetResponse sets the response code, content type and data.
	SetResponse(code codes.Code, contentType string, data []byte) error

	// WriteErrors returns true if errors should be written to the client.
	WriteErrors() bool
}
