package server

import (
	"time"

	"github.com/silkeh/senml"
)

// Backend represents a backend for the Cloudburst server.
type Backend interface {
	// Query retrieves SenML data from the backend with the given parameters.
	Query(name, host string, from, to time.Time, last bool) (measurements []senml.Measurement, err error)

	// Write writes SenML data to the backend.
	Write(name, host string, data ...senml.Measurement) error

	// Close closes the connection to the backend.
	Close() error
}
