package server

import (
	"bytes"
	"fmt"
	"net"
	"net/url"
	"strings"

	"github.com/plgd-dev/go-coap/v2/message"
	"github.com/plgd-dev/go-coap/v2/message/codes"
	"github.com/plgd-dev/go-coap/v2/mux"
)

// mimeTypes contains a mapping of the MIME type strings to CoAP media types.
var mimeTypes = map[string]message.MediaType{
	TypeCBOR:      message.AppCBOR,
	TypeJSON:      message.AppJSON,
	TypeXML:       message.AppXML,
	TypeSenMLCBOR: 112,
	TypeSenMLJSON: 110,
	TypeSenMLXML:  310,
}

// CoAPResponseWriter is a CoAP based ResponseWriter.
type CoAPResponseWriter struct {
	mux.ResponseWriter
}

// NewCoAPResponseWriter creates a CoAP based ResponseWriter.
func NewCoAPResponseWriter(w mux.ResponseWriter) ResponseWriter {
	return &CoAPResponseWriter{w}
}

// SetResponse sets the response code, content type and data
func (r *CoAPResponseWriter) SetResponse(code codes.Code, contentType string, data []byte) error {
	f, ok := mimeTypes[contentType]
	if !ok {
		return fmt.Errorf("invalid content type %q", contentType)
	}

	return r.ResponseWriter.SetResponse(code, f, bytes.NewReader(data))
}

// WriteErrors returns true if errors should be written to the client.
func (r *CoAPResponseWriter) WriteErrors() bool {
	return false
}

// NewCoAPRequest creates Request based on a coap.Request.
func NewCoAPRequest(r *mux.Message, c mux.Client) *Request {
	queries, _ := r.Message.Options.Queries()
	query, _ := url.ParseQuery(strings.Join(queries, "&"))
	request := &Request{
		Method:      r.Message.Code.String(),
		RemoteAddr:  coapRemoteAddr(c),
		Query:       query,
		Body:        r.Message.Body,
		Path:        make([]string, 8),
		Confirmable: r.IsConfirmable,
	}

	n, _ := r.Message.Options.GetStrings(message.URIPath, request.Path)
	request.Path = request.Path[:n]

	format, err := r.Message.Options.ContentFormat()
	if err == nil {
		request.ContentType = findMimeType(format)
	}

	accept, err := r.Message.Options.Accept()
	if err == nil {
		request.Accept = findMimeType(accept)
	}

	return request
}

// coapRemoteAddr returns the remote address for a coap.Request.
func coapRemoteAddr(c mux.Client) string {
	switch a := c.RemoteAddr().(type) {
	case *net.IPAddr:
		return a.IP.String()
	case *net.UDPAddr:
		return a.IP.String()
	case *net.TCPAddr:
		return a.IP.String()
	default:
		return a.String()
	}
}

// findMimeType converts a media type to a MIME type string.
// This is similar to message.MediaType.String(), except it doesn't
// add a human-readable suffix, and returns only the types CloudBurst supports.
func findMimeType(mediaType message.MediaType) string {
	for s, m := range mimeTypes {
		if m == mediaType {
			return s
		}
	}
	return TypeAny
}
