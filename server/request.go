package server

import (
	"fmt"

	"io"
	"io/ioutil"
	"net"
	"net/url"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/plgd-dev/go-coap/v2/message/codes"
	"github.com/rs/zerolog/log"
	"github.com/silkeh/senml"
)

// DefaultMeasurementName contains the measurement name when none is given in the request.
const DefaultMeasurementName = "cloudburst"

// DefaultFromTime contains the value for the 'from' parameter.
const DefaultFromTime = -1 * time.Hour

// DefaultToTime contains the value for the 'to' parameter.
const DefaultToTime = 0 * time.Second

// MeasurementNameRegex contains the regular expression used for validating measurements.
var MeasurementNameRegex = regexp.MustCompile(`^\w+$`)

// Supported MIME types.
const (
	TypeAny         = "*/*"
	TypePlainText   = "text/plain"
	TypeApplication = "application/*"
	TypeCBOR        = "application/cbor"
	TypeJSON        = "application/json"
	TypeXML         = "application/xml"
	TypeSenMLCBOR   = "application/senml+cbor"
	TypeSenMLJSON   = "application/senml+json"
	TypeSenMLXML    = "application/senml+xml"
)

// Request represents a generic API request.
type Request struct {
	Method      string
	RemoteAddr  string
	Path        []string
	Query       url.Values
	ContentType string
	Accept      string
	Body        io.Reader
	Confirmable bool
}

// Measurement returns the measurement name for the request.
func (r *Request) Measurement() (string, error) {
	if len(r.Path) == 2 {
		return DefaultMeasurementName, nil
	}
	if len(r.Path) < 3 || len(r.Path) > 4 {
		return "", fmt.Errorf("invalid path %q", strings.Join(r.Path, "/"))
	}

	name := r.Path[2]
	if !MeasurementNameRegex.MatchString(name) {
		return "", fmt.Errorf("invalid name %q", name)
	}

	return name, nil
}

// RequestHost returns the `host` for this request from either
// the path, or the remote address.
func (r *Request) RequestHost() (string, error) {
	host := r.PathHost()
	if host != "" {
		return host, nil
	}

	names, err := net.LookupAddr(r.RemoteAddr)
	if err != nil || len(names) == 0 {
		return r.RemoteAddr, err
	}

	names = strings.SplitN(names[0], ".", 2)
	return names[0], nil
}

// SenML returns the SenML data encoded in the body.
func (r *Request) SenML() (measurements []senml.Measurement, err error) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, err
	}

	switch r.ContentType {
	case TypeJSON, TypeSenMLJSON:
		measurements, err = senml.DecodeJSON(body)
	case TypeCBOR, TypeSenMLCBOR:
		measurements, err = senml.DecodeCBOR(body)
	case TypeXML, TypeSenMLXML:
		measurements, err = senml.DecodeXML(body)
	default:
		err = fmt.Errorf("unsupported content type %q", r.ContentType)
	}
	return
}

// PathHost returns the `host` value in the request path.
func (r *Request) PathHost() string {
	if len(r.Path) == 4 {
		return r.Path[3]
	}
	return ""
}

// QueryLast returns the `last` value in the query.
func (r *Request) QueryLast() bool {
	_, ok := r.Query["last"]
	return ok
}

// QueryTime returns the `from` and `to` (time) values in the query.
func (r *Request) QueryTime() (from, to time.Time, err error) {
	from, err = r.parseTime(r.Query.Get("from"), time.Now().Add(DefaultFromTime))
	if err != nil {
		return
	}

	to, err = r.parseTime(r.Query.Get("to"), time.Now().Add(DefaultToTime))
	return
}

// parseTime parses a given time from the query, or returns a default time when empty.
func (r *Request) parseTime(s string, def time.Time) (time.Time, error) {
	if s == "" {
		return def, nil
	}
	if i, err := strconv.ParseInt(s, 10, 64); err == nil {
		return time.Unix(i, 0), nil
	}

	d, err := time.ParseDuration(s)
	if err != nil {
		return time.Time{}, err
	}

	return time.Now().Add(-d), nil
}

func (r *Request) log(msg string) {
	log.Debug().
		Str("remote", r.RemoteAddr).
		Str("method", r.Method).
		Strs("path", r.Path).
		Msg(msg)
}

func (r *Request) logResult(msg string, code codes.Code, data []byte) {
	log.Info().
		Str("remote", r.RemoteAddr).
		Str("method", r.Method).
		Strs("path", r.Path).
		Str("code", code.String()).
		Int("resp_len", len(data)).
		Msg(msg)
}
