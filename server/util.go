package server

import (
	"fmt"
	"sort"
	"strconv"
	"strings"
)

// acceptable contains all acceptable MIME types.
var acceptable = []string{
	TypeCBOR, TypeSenMLCBOR,
	TypeJSON, TypeSenMLJSON,
	TypeXML, TypeSenMLXML,
	TypeApplication, TypeAny,
}

// acceptPart represents an element of an Accept header.
type acceptPart struct {
	s string
	q float64
}

type acceptParts []*acceptPart

// Len is part of sort.Interface.
func (s acceptParts) Len() int {
	return len(s)
}

// Swap is part of sort.Interface.
func (s acceptParts) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

// Less is part of sort.Interface.
func (s acceptParts) Less(i, j int) bool {
	return s[i].q < s[i].q
}

// findAccept finds the most acceptable MIME type.
func findAccept(hdr, content string) (t string, err error) {
	if hdr == "" {
		if content == "" {
			return TypeAny, nil
		}
		// fall back to content type
		hdr = content
	}

	values := strings.Split(hdr, ",")
	accepts := make(acceptParts, len(values))

	for i, v := range values {
		accepts[i], err = parseAcceptPart(v)
		if err != nil {
			return TypeAny, err
		}
	}

	sort.Sort(accepts)

	for _, a := range accepts {
		for _, b := range acceptable {
			if a.s == b {
				return a.s, nil
			}
		}
	}

	return TypeAny, fmt.Errorf("cannot provide %q", hdr)
}

// parseAcceptPart parses a part of an Accept header into an acceptPart.
func parseAcceptPart(part string) (*acceptPart, error) {
	parts := strings.SplitN(strings.TrimSpace(part), ";q=", 2)
	if len(parts) == 1 {
		return &acceptPart{s: parts[0], q: 1}, nil
	}

	q, err := strconv.ParseFloat(parts[1], 64)
	if err != nil {
		return nil, err
	}

	return &acceptPart{s: parts[0], q: q}, nil
}
