package server

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io"

	"github.com/plgd-dev/go-coap/v2/message/codes"
	"github.com/rs/zerolog/log"
	"github.com/ugorji/go/codec"
)

// errorNotFound contains the default 'not found' error.
var errorNotFound = NewErrorf(codes.NotFound, "not found")

// errorMessage represents the message returned to the client.
type errorMessage struct {
	XMLName xml.Name `json:"-" xml:"error-message" codec:"-"`
	Error   string   `json:"error" xml:"error" codec:"error"`
}

// Error is a generic request/response error.
type Error struct {
	// The error causing this error.
	Err error

	// The corresponding response code.
	Code codes.Code
}

// NewError returns a new Error.
func NewError(code codes.Code, err error) *Error {
	return &Error{Err: err, Code: code}
}

// NewErrorf returns a new error with a custom description.
func NewErrorf(code codes.Code, format string, a ...interface{}) *Error {
	return &Error{Err: fmt.Errorf(format, a...), Code: code}
}

// Error returns the error text.
func (e *Error) Error() string {
	return fmt.Sprintf("%s (%v)", e.Err, e.Code)
}

// message returns the errorMessage for this Error.
func (e *Error) message() *errorMessage {
	return &errorMessage{Error: e.Err.Error()}
}

// Write writes the for a Request to a ResponseWriter.
func (e *Error) Write(w ResponseWriter, req *Request) {
	var data []byte
	accept, err := findAccept(req.Accept, req.ContentType)
	if err != nil {
		log.Warn().Err(err).Msg("Error finding acceptable type")
		err = nil
	}

	switch accept {
	case TypeCBOR:
		err = codec.NewEncoderBytes(&data, &codec.CborHandle{}).Encode(e.message())
	case TypeJSON, TypeApplication, TypeAny:
		accept = TypeJSON
		data, err = json.Marshal(e.message())
	case TypeXML:
		data, err = xml.Marshal(e.message())
	default:
		accept = TypePlainText
		data = []byte("Error: " + e.Err.Error())
	}
	if err != nil {
		log.Error().Err(err).Msg("Error creating error")
		return
	}

	req.logResult("Request", e.Code, data)
	err = w.SetResponse(e.Code, accept, data)
	if err != nil {
		log.Error().Err(err).Msg("Error returning error")
	}
}

// writeCbor serializes an interface to an io.Writer.
func writeCbor(w io.Writer, i interface{}) (int, error) {
	var cbor codec.CborHandle
	var b []byte
	err := codec.NewEncoderBytes(&b, &cbor).Encode(i)
	if err != nil {
		return 0, err
	}

	return w.Write(b)
}
