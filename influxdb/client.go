package influxdb

import (
	"fmt"
	"time"

	client "github.com/influxdata/influxdb1-client/v2"
	"github.com/silkeh/senml"
)

// ClientConfig contains InfluxDB client configuration
type ClientConfig struct {
	Host, Database, Username, Password string
}

// Client is an extended InfluxDB client
type Client struct {
	client.Client
	Database          string
	batchPointsConfig client.BatchPointsConfig
}

// NewClient returns a Client for the given configuration
func NewClient(config *ClientConfig) (c *Client, err error) {
	c = &Client{
		Database: config.Database,
		batchPointsConfig: client.BatchPointsConfig{
			Database: config.Database,
		},
	}
	c.Client, err = client.NewHTTPClient(client.HTTPConfig{
		Addr:     config.Host,
		Username: config.Username,
		Password: config.Password,
	})
	return
}

// writePoints writes a series of points to InfluxDB.
func (c *Client) writePoints(rp string, points ...*client.Point) error {
	bp, err := client.NewBatchPoints(c.batchPointsConfig)
	if err != nil {
		return err
	}
	if rp != "" {
		bp.SetRetentionPolicy(rp)
	}

	bp.AddPoints(points)
	return c.Client.Write(bp)
}

// Write writes a series of SenML measurements to InfluxDB.
func (c *Client) Write(name, host string, data ...senml.Measurement) error {
	points, err := PointsFromSenML(name, host, data)
	if err != nil {
		return err
	}

	return c.writePoints("", points...)
}

// query executes a query and returns the result
func (c *Client) query(q client.Query) (res []client.Result, err error) {
	response, err := c.Client.Query(q)
	if err != nil {
		return nil, err
	}

	if response.Error() != nil {
		return nil, response.Error()
	}

	return response.Results, nil
}

// Query builds a query based on the given parameters and returns the result as SenML.
func (c *Client) Query(name, host string, from, to time.Time, last bool) (measurements []senml.Measurement, err error) {
	queries := c.buildQueries(name, host, from, to, last)
	for _, query := range queries {
		results, err := c.query(query)
		if err != nil {
			return nil, err
		}

		for _, row := range results[0].Series {
			m, err := NewSenML(&row)
			if err != nil {
				return nil, err
			}

			measurements = append(measurements, m...)
		}
	}

	return
}

// buildQueries builds queries based on the given parameters.
func (c *Client) buildQueries(name, host string, from, to time.Time, last bool) []client.Query {
	if last {
		queries := make([]client.Query, len(fields))
		for i, field := range fields {
			queries[i] = c.buildQuery(name, host, field, from, to, last)
		}
		return queries
	}

	return []client.Query{c.buildQuery(name, host, "", from, to, last)}
}

// lastQuery contains the query to get the last values for a field and host.
const lastQuery = `SELECT last(%s) AS %s FROM %s WHERE time >= $from AND time <= $to`

// allQuery contains the query to get all values for a host.
const allQuery = `SELECT * FROM %s WHERE time >= $from AND time <= $to`

// fields contains all possible value fields
var fields = []string{BooleanField, DataField, SumField, StringField, ValueField}

func (c *Client) buildQuery(name, host, field string, from, to time.Time, last bool) client.Query {
	hostQuery := ""
	if host != "" {
		hostQuery = " AND host = $host"
	}

	params := client.Params{
		"host": host,
		"from": from.UnixNano(),
		"to":   to.UnixNano(),
	}

	var query string
	if last {
		query = fmt.Sprintf(lastQuery+hostQuery+" GROUP BY *", field, field, name)
	} else {
		query = fmt.Sprintf(allQuery+hostQuery, name)
	}

	return client.NewQueryWithParameters(query, c.Database, "ns", params)
}
