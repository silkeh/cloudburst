module gitlab.com/silkeh/cloudburst

go 1.14

require (
	github.com/dsnet/golib/memfile v1.0.0 // indirect
	github.com/influxdata/influxdb-client-go/v2 v2.5.1
	github.com/influxdata/influxdb1-client v0.0.0-20200827194710-b269163b24ab
	github.com/influxdata/line-protocol v0.0.0-20210922203350-b1ad95c89adf // indirect
	github.com/pion/dtls/v2 v2.0.10-0.20210502094952-3dc563b9aede
	github.com/pion/logging v0.2.2
	github.com/plgd-dev/go-coap/v2 v2.4.1-0.20210517130748-95c37ac8e1fa
	github.com/plgd-dev/kit v0.0.0-20210614190235-99984a49de48 // indirect
	github.com/rs/zerolog v1.25.0
	github.com/silkeh/senml v0.0.0-20210422111947-b6df6a0d383f
	github.com/ugorji/go/codec v1.2.6
	go.uber.org/atomic v1.9.0 // indirect
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519 // indirect
	golang.org/x/net v0.0.0-20210924151903-3ad01bbaa167 // indirect
	golang.org/x/sys v0.0.0-20210925032602-92d5a993a665 // indirect
	gopkg.in/yaml.v2 v2.4.0
)
